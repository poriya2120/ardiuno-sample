/*
SanatBazar

Author: poriyadaliry


*/

#include <SoftwareSerial.h>
#include <Sim800l.h>
#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);
//SoftwareSerial SIM800(2, 3); //SIM800L Tx >> 2 & Rx >> 3
//String number = "+989396392269";
int pirPin = 8;
int lastState = LOW;
int buzzer = 7;
Sim800l SIM800;
char *myNumber = "+989396392269";
char *SMS = "Hello, I am Arduino Uno.\nNice to meet you!";
//void sendSMS()
//{
//  SIM800.println("AT+CMGF=1");
//  delay(1000);
//  SIM800.println("AT+CMGS=\"" + number + "\"\r");
//  delay(1000);
//  String SMS = "there is human in your home \n you must come back to home";
//  SIM800.println(SMS);
//  delay(100);
//  SIM800.println((char)26);
//  delay(1000);
//}

//void callMe()
//{
//  SIM800.print(F("ATD"));
//  SIM800.print(number);
//  SIM800.print(F(";\r\n"));
//}

void setup()
{
  pinMode(pirPin, INPUT);
  digitalWrite(pirPin, LOW);
  delay(5000);
//  SIM800.begin(9600);
//  delay(1000);
  SIM800.begin();
  pinMode(buzzer, OUTPUT);

  //----
   Serial.begin(9600);   // Initiate a serial communication
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  Serial.println("Approximate your card to the reader...");
  Serial.println();

}
void loop()
{
  if (digitalRead(pirPin))
  {
    if (!lastState)
    {
if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Message : ");
  content.toUpperCase();
  ///---------
   digitalWrite(buzzer, HIGH);
         
    ///--------------
  if (content.substring(1) == "BD 31 15 2B") //change here the UID of the card/cards that you want to give access
  {




    
    Serial.println("Authorized access");
    Serial.println();
    delay(3000);
  }
 
 else   {
         delay(10000);
          SIM800.sendSms(myNumber, SMS);
                delay(15000);
          //      callMe();
            SIM800.callNumber(myNumber);
                lastState = !lastState;
  }
}
      ///-----------
//      sendSMS();

    }
  }
  else
  {
    if (lastState)
    {
      lastState = !lastState;
    }
  }
}
